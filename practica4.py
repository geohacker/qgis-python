
'''Seleccion de Elementos y Zoom'''

from PyQt5.QtGui import *

class practica4:
    def __init__(self):
        #Change the color of the selection
        iface.mapCanvas().setSelectionColor(QColor("red"))
        #iface.mapCanvas().setSelectionColor(QColor(255,0,0))
        #Get the active layer (must be a vector layer)
        self.layer = iface.activeLayer()
    
    def seleccionarTodos(self):
        # Selecting all the features of a layer
        feature= self.layer.selectAll()
    
    def seleccionarPrimero(self):
        #Selecting the first feature of a layer 
        selected_fid = []
        #Get the first feature id from the layer 
        for feature in self.layer.getFeatures():
            selected_fid. append(feature.id()) 
            break
        # Add these features to the selected list 
        self.layer.select(selected_fid)
    
    def seleccionarExpresion(self):
        # Selecting features by expression
        expr = "\"MAN_COD\" = '13' AND \"AREA\" >= 60"
        self.layer.selectByExpression(expr,QgsVectorLayer.SetSelection)
    
    def seleccionarlimpiar(self):
        # Cleaning selection 
        self.layer.removeSelection ()
    
    def ZoomToSeleccion(self):
        canvas = qgis.utils.iface.mapCanvas() 
        canvas.zoomToSelected(self.layer)
        canvas.refresh()
        print (canvas.scale())
    
    def ZoomToEscala(self, escala):
        canvas = qgis.utils.iface.mapCanvas() 
        canvas.zoomScale(escala) 
        #zoomByFactor(doublé FactordeEscala) 
        #zoomln()
        #zoomüut() 
        #zoomToFullExtent() 
        #zoomToNextExtent() 
        #zoomToPreviousExtent()
        #zoomWithCenter(int x, int y, bool zoomln)
        print(canvas.scale())



instpractica4 = practica4() 
#instpractica4.seleccionarTodos()
#instpractica4.seleccionarPrimero() 
#instpractica4.seleccionarExpresion() 
#instpractica4.seleccionarlimpiar() 
#instpractica4.ZoomToSeleccion()
instpractica4.ZoomToEscala(1000)



'''
1.Pruebe los métodos seleccionarTodos, seleccionarPrimero, seleccionarlimpiar y ZoomToEscala.
2.	Cree una clase que ayude a realizar selección sobre una capa o modifique la clase existente, enviando como parámetros la capa vectorial, nombre del campo, valor o valores y método de comparación .
3.	Cree un método en la clase, trabajada en el punto 2, que realice un acercamiento a los elementos seleccionados a una escala cerrada (por ejemplo 200, 250, 300, 500, 700, etc.)


'''