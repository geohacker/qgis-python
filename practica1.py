shp=iface.addVectorLayer('C:/Users/57300/Documents/Interfaces/datos2016III/loteo.shp','Lotes','ogr')

if not shp:
    print("El layer no se pudo cargar")
    
'''Examinando los features de la capa'''
#Lista de Features
features = shp.getFeatures()
for feature in features: 
    #manejo de geometrías
    geom = feature.geometry()
    print("Feature ID: ", feature.id())
    #show sorne information about the feature
    if geom.wkbType() == QgsWkbTypes.Point:
        x = geom.asPoint()
        print("Point:", x)
    elif geom.wkbType() == QgsWkbTypes.LineString:
        x = geom.asPolyline()
        print('Line:', x, 'points',  'length:',  geom.length()) 
    elif geom.wkbType() == QgsWkbTypes.MultiPolygon:
        x = geom.asMultiPolygon()
        print("Polygon:", x, "Area: ", geom.area()) 
    else:
        print("Unknown") 
        print(geom. wkbType())

'''Examinando los atributos de la capa'''
print("-ATRIBUTOS --")
for field in shp.fields(): 
    print(field.name(), field.typeName())
'''Examinando los valores atributivos de la capa'''
print("-- VALORES ATRIBUTIVOS--")
features = shp.getFeatures()
for feature in features:
    attrs = feature.attributes()
    # attrs is a list. It contains all the attribute values of this feature 
    print(attrs)


'''
Pagina: https://sites.google.com/site/interfacessig/
1.	Adicionar código que permita ver cada una de las características de los atributos de cualquier capa vectorial adicionada por medio de código. (realice una clase que le ayude a realizar de una forma más eficiente este proceso)
2.	Adicionar código que permita ver los datos espaciales de cada feature de cualquier capa vectorial adicionada por medio de código. (realice una clase que permita realizar este proceso de manera más eficiente - soportar las geometrías      no     tenidas     en     cuenta      dentro     de     la     práctica  ( https://qgis.org/api/classQgsWkbTypes.html)).
'''
