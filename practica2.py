shp=iface.addVectorLayer('C:/Users/57300/Documents/Interfaces/datos2016III/loteo.shp','Lotes','ogr')

if not shp:
    print("El layer no se pudo cargar")
    
'''Mensaje y visibilidad de capas'''

for layer in QgsProject.instance().mapLayers().values():
    print("--LAYER--")
    print(layer.name()) 
    print(layer.type())
    print(layer.dataProvider().dataSourceUri())
    print(QgsProject.instance().layerTreeRoot().findLayer(layer.id()).itemVisibilityChecked())
    #QgsProject.instance().layerTreeRoot().findLayer(layer.id()).setitemVisibilityChecked(True) 
   

layer = iface.activeLayer()

QMessageBox.information(iface.mainWindow(),"Capa Activa", "[información] La capa se apagara",)
QMessageBox.warning(iface.mainWindow(),"Capa Activa",'[Warning] La capa  se apagara ')
QMessageBox.critical(iface .mainWindow(),"Capa Activa",'[Critical] La capa se apagara ')
#iface.messageBar().pushMessage("Capa Activa",'La capa se apagara', Qgis.lnfo,5)

QgsProject.instance().layerTreeRoot().findLayer(layer.id()).setItemVisibilityChecked(False)


'''
1.Adicionar código que permita aleatoriamente modificar la visibilidad de las capas y
que previamente informe al usuario el cambio que se va a realizar (el tipo de mensaje 
debe ser aleatorio) (realice una clase que permita realizar este proceso).

'''