class salidas:
    def crear(self):
        project = QgsProject.instance()
        #get a reference to the layout manager 
        manager = project.layoutManager() 
        #make a new print layout object
        layout = QgsPrintLayout(project) 
        layoutName = "PrintLayout" 
        layouts_list = manager.printLayouts()

        for layout in layouts_list:
            if layout.name() == layoutName: 
                manager.removeLayout(layout)

        layout = QgsPrintLayout(project)
        #needs to call this according to API documentaiton 
        layout.initializeDefaults() 
        layout.setName(layoutName)
        layout_page = QgsLayoutItemPage(layout) 
        layout_page.setPageSize(QgsLayoutSize(210, 297, QgsUnitTypes.LayoutMillimeters)) 
        layout_page.decodePageOrientation('Portrait')
        #add layout to manager
         
        manager.addLayout(layout)

        #ADD MAP
        #create default map canvas
        canvas = qgis.utils.iface.mapCanvas() 
        rect = QgsRectangle(canvas.fullExtent()) 
        rect.scale(1.2)
        canvas.setExtent(rect)
        layout_map = QgsLayoutItemMap.create(layout) 
        layout_map.setId("Mapa01")
        #using ndawson's answer below, do this before setting extent 
        layout_map.attemptResize(QgsLayoutSize(4,4, QgsUnitTypes.LayoutInches)) 
        layout_map.attemptMove(QgsLayoutPoint(3.5, 1.25, QgsUnitTypes.LayoutInches)) 
        #set an extent
        layout_map.setExtent(rect) 
        #add the map to the layout
        layout.addLayoutItem(layout_map)

        #Add a legend to the print layout 
        legend = QgsLayoutItemLegend(layout) 
        legend.setTitle("Leyenda") 
        legend.setAutoUpdateModel(False) 
        legItems=legend.model() 
        layout.addItem(legend)

        #Create an object of QgsComposerLabel class 
        label = QgsLayoutItemLabel(layout) 
        label.setId("Titulo")
        label.setItemRotation(0, True) 
        label.setFont(QFont('Tahoma', 11)) 
        label.setFontColor(QColor('white')) 
        label.setText("MI PRIMER MAPA CON PYQGIS")
        label.adjustSizeToText() 
        #label.setFrameEnabled(True) 
        label.setBackgroundEnabled(True) 
        label.setBackgroundColor(QColor('blue'))
        label.attemptMove(QgsLayoutPoint(5, 0.25, QgsUnitTypes.LayoutInches)) 
        layout.addItem(label)

        #Create an object of QgsLayoutItemScaleBar 
        scalebar = QgsLayoutItemScaleBar(layout) 
        scalebar.setStyle('Single Box') 
        #scalebar.setStyle('Line Ticks Up')
         
        scalebar.setUnits(QgsUnitTypes.DistanceKilometers) 
        scalebar.setNumberOfSegments(3) 
        scalebar.setNumberOfSegmentsLeft(1) 
        scalebar.setUnitsPerSegment(100) 
        scalebar.setUnitLabel('km') 
        scalebar.setFont(QFont('Arial', 14)) 
        scalebar.setLinkedMap(layout_map) 
        #scalebar.applyDefaultSize()
        scalebar.update()
        scalebar.attemptMove(QgsLayoutPoint(20, 145, QgsUnitTypes.LayoutMillimeters)) 
        layout.addItem(scalebar)

        # add imagen
        logo = QgsLayoutItemPicture(layout) 
        logo.setPicturePath(r"C:\Users\57300\Documents\Interfaces\imgs\ud.png") 
        logo.attemptResize(QgsLayoutSize(23, 23,QgsUnitTypes.LayoutMillimeters)) 
        logo.attemptMove(QgsLayoutPoint(125,125,QgsUnitTypes.LayoutMillimeters)) 
        layout.addLayoutItem(logo)

        # add some items
        item1 = QgsLayoutItemShape(layout) 
        item1.attemptSetSceneRect(QRectF(10, 20, 10, 15))
        item1.attemptMove(QgsLayoutPoint(30, 95, QgsUnitTypes.LayoutMillimeters)) 
        fill = QgsSimpleFillSymbolLayer()
        fill_symbol = QgsFillSymbol() 
        fill_symbol.changeSymbolLayer(0, fill) 
        fill.setColor(Qt.green) 
        fill.setStrokeStyle(Qt.NoPen) 
        item1.setSymbol(fill_symbol) 
        layout.addItem(item1)

        polygon1 = QPolygonF() 
        polygon1.append(QPointF(200.0, 100.0))
        polygon1.append(QPointF(220.0, 100.0))
        polygon1.append(QPointF(220.0, 115.0))
        polygon1.append(QPointF(200.0, 115.0))
        layoutItemPolygon = QgsLayoutItemPolygon(polygon1, layout) 
        layout.addLayoutItem(layoutItemPolygon)

        polyline1 = QPolygonF() 
        polyline1.append(QPointF(200.0, 150.0))
        polyline1.append(QPointF(220.0, 160.0))
        polyline1.append(QPointF(220.0, 120.0))
         
        layoutItemPolyline = QgsLayoutItemPolyline(polyline1, layout)
        layout.addLayoutItem(layoutItemPolyline)


        #exportando el Layout # 	
        layout_exporter = QgsLayoutExporter(layout)

        image_export_settings = QgsLayoutExporter.ImageExportSettings() 
        image_export_settings.dpi = 200
        image_export_settings.imageSize = QSize(2000, 2000) # pixels
        res = layout_exporter.exportToImage('C:/Users/57300/Documents/Interfaces/Test.png',image_export_settings)
        if res != QgsLayoutExporter.Success: 
            raise RuntimeError()

        pdf_export_settings = QgsLayoutExporter.PdfExportSettings() 
        pdf_export_settings.dpi = 200
        res = layout_exporter.exportToPdf("C:/Users/57300/Documents/Interfaces/Test.pdf", pdf_export_settings)
        if res != QgsLayoutExporter.Success: 
            raise RuntimeError()

    def modifica(self):
        manager = QgsProject.instance().layoutManager() 
        layout = manager.layoutByName("PrintLayout") 
        print("Elementos del Mapa")
        for elem in layout.items(): 
            print (elem)
            if isinstance(elem, QgsLayoutItemMap): 
                print(type(elem))
                print(elem.extent()) 
                print(elem.layers()) 
                print(elem.crs()) 
                print(elem.numberExportLayers()) 
                print(elem.displayName())

        map = layout.itemById('Mapa01') 
        map.attemptSetSceneRect(QRectF(0, 0, 100, 100)) 
        #map.setCrs(QgsCoordinateReferenceSystem.fromEpsgId(3575))
        map.attemptMove(QgsLayoutPoint(3.5, 1.25, QgsUnitTypes.LayoutInches))
        map.setExtent(QgsRectangle(98468, 117899, 98519, 117945))
         
        titulo = layout.itemById('Titulo') 
        titulo.setFont(QFont('Arial')) 
        titulo.setFontColor(QColor('red')) 
        titulo.setText("TITULO CON PYQGIS")


prueba = salidas() 
prueba.crear() 
#prueba.modifica()
