
'''M'''

class practica5:
    def __init__(self):
        # Get the active layer (must be a vector layer ) 
        self.layer = iface.activeLayer()

    def TipoSimbologia(self):
        print (self.layer.geometryType()) 
        renderer = self.layer.renderer() 
        print ("Tipo:", renderer.type())
        print(renderer.dump())
        print(renderer.symbol().symbolLayers()[0]. properties())
        print(renderer.symbol().symbolLayerCount())
        print (renderer.symbol())

    def SimboloSimple(self):
        Geo = self.layer.geometryType() 
        if Geo == 0:
            self.layer.renderer().symbol().symbolLayer(0).setSize(3)
            props = self.layer.renderer().symbol().symbolLayer(0).properties()
            props[' color '] = 'yellow'
            props['name'] = 'square'
            self.layer.renderer().setSymbol(QgsMarkerSymbol.createSimple(props))
        elif Geo == 1:
            simbolo = self.layer.renderer().symbol().symbolLayer(0) 
            simbolo.setColor(QColor(2551,0,0))
            simbolo.setWidth(1)
            
        elif Geo == 2:
            simbolo = self.layer.renderer().symbol().symbolLayer(0) 
            simbolo.setColor(QColor("Transparent"))
            props = self.layer.renderer().symbol().symbolLayer(0).properties()
            props['color_border' ] = 'yellow' 
            props['width_border' ] = '1'
            props["style" ] = "solid"
            props["style_border" ] = "dot " 
            #props["joinstyle" ] = "wine"
            self.layer.renderer().setSymbol(QgsFillSymbol.createSimple(props))

        #show the changes
        self.layer.triggerRepaint ()
        iface.layerTreeView().refreshLayerSymbology(self.layer.id())

    def SimboloCategoria(self): 
        lista = {
        "01": ('green', 'Manzana 01'),
        "02": ('yellow', 'Manzana 02'),
        "03": ('red', 'Manzana 03'),
        "04": ('orange', 'Manzana 04'),
        "05": ('blue', 'Manzana 05'),
        "06": ('magenta', 'Manzana 06'),
        "07": ('gray', 'Manzana 07'),
        }
        
        ranges = []
        for valor, (color, label) in lista.items():
            symbol = QgsSymbol.defaultSymbol(self.layer.geometryType()) 
            symbol.setColor(QColor(color))
            rng = QgsRendererCategory(valor, symbol, label)
            ranges.append(rng)
        campo = "MAN_COD"
        myRenderer = QgsCategorizedSymbolRenderer(campo,ranges)
        self.layer.setRenderer(myRenderer)
        self.layer.triggerRepaint()
        iface.layerTreeView().refreshLayerSymbology(self.layer.id())

    def SimboloGradual(self):
        lista = {
        ('Bajo(O - 80 M2)', 0, 500, 'green'),
        ('Medio(80 - 200 M2)', 500, 1500, 'yellow'),
        ('Alto (200 - 338 M2)', 1500, 4000, 'orange'),
        }
        
        ranges =[]
        for label, lower, upper, color in lista:
            symbol = QgsSymbol.defaultSymbol(self.layer.geometryType()) 
            symbol.setColor(QColor(color))
            symbol.setOpacity(0.5)
            rng = QgsRendererRange(lower, upper, symbol, label) 
            ranges.append(rng)
        campo = "AREA"
        myRenderer = QgsGraduatedSymbolRenderer(campo, ranges)
        myRenderer.setMode(QgsGraduatedSymbolRenderer.EqualInterval) 
        self.layer.setRenderer(myRenderer)
        self.layer.triggerRepaint()
        iface.layerTreeView().refreshLayerSymbology(self.layer.id())



print(QgsApplication.rendererRegistry().renderersList ()) 
instpractica5 = practica5()
#instpractica5.TipoSimbologia() 
#instpractica5.SimboloSimple()
#instpractica5.SimboloGradual() #Ejecutar con polígonos - capa Manzana 
instpractica5.SimboloCategoria() #Ejecutar con polígonos - capa Loteo

'''
Cree una clase que reciba como parámetro el atributo por el cual se desea realizar una clasificación por único valor, los colores deben ser aleatorios.
2.	Cree una clase que reciba como parámetro el atributo por el cual se desea realizar una clasificación por rangos, los colores deben ser aleatorios.
3.	Entienda como puede leer las propiedades de la simbologíade una capa.
'''


