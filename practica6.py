
'''Manejo de datos vectoriales en QGIS con Python - Labels y Filtros'''
class textos:
    def metl(self):
        layer = iface.activeLayer()
        
        text_format = QgsTextFormat()
        text_format.setFont(QFont ("Arial", 12))
        text_format.setSize(12)
        
        buffer_settings = QgsTextBufferSettings()
        buffer_settings.setEnabled(True)
        buffer_settings.setSize(0.1)
        buffer_settings.setColor(QColor("black"))

        text_format.setBuffer(buffer_settings)

        layer_settings = QgsPalLayerSettings()
        layer_settings.setFormat(text_format)
        
        layer_settings.fieldName = "MAN_COD"
        layer_settings.placement = 4
        layer_settings.enabled = True
        layer_settings = QgsVectorLayerSimpleLabeling(layer_settings)
        layer.setLabelsEnabled(True)
        layer.setLabeling(layer_settings)
        layer.triggerRepaint()
        iface.mapCanvas().refresh()

    def met2(self):
        layer = iface.activeLayer() 
        layer.setSubsetString("\"MAN_COD\" = '04'") 
        layer.triggerRepaint()
        iface.mapCanvas().refresh()

prueba = textos() 
#prueba.metl() 
prueba.met2()


'''
1. Modifique el código para mostrar en las etiquetas un texto "Manzana XX", 
donde XX corresponda al código existente en el campo MAN_COD.
2. Entienda como  puede  leer las propiedades de ubicación y texto, 
en la simbología de etiquetas de una capa.

'''


